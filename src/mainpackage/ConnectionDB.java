package mainpackage;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Abrenica, Aljun
 * C R U D 
 */
public class ConnectionDB {
    private static String dbUrl = "jdbc:mysql://localhost/mydatabase";
    private static  String user = "root";
    private static String pass = "";
    public static Connection connDb(){
            try{
                Connection conn = DriverManager.getConnection(dbUrl,user,pass);
                return conn;
            }catch(Exception e){
                JOptionPane.showMessageDialog(null,e);
                return null;
            }
    }
}
